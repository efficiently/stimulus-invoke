(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.StimulusInvoke = {}));
})(this, (function (exports) {
  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _finallyRethrows(body, finalizer) {
    try {
      var result = body();
    } catch (e) {
      return finalizer(true, e);
    }

    if (result && result.then) {
      return result.then(finalizer.bind(null, false), finalizer.bind(null, true));
    }

    return finalizer(false, result);
  }

  class InternTestCase {
    static registerSuite() {
      return intern.getInterface('object').registerSuite(this.name, {
        tests: this.tests
      });
    }

    static get tests() {
      return this.testNames.reduce((tests, testName) => {
        return _extends({}, tests, {
          [testName]: internTest => this.runTest(internTest)
        });
      }, {});
    }

    static get testNames() {
      return this.testKeys.map(key => key.slice(5));
    }

    static get testKeys() {
      return Object.getOwnPropertyNames(this.prototype).filter(key => key.match(/^test /));
    }

    static runTest(internTest) {
      var testCase = new this(internTest);
      return testCase.runTest();
    }

    constructor(internTest) {
      this.internTest = internTest;
    }

    get testName() {
      return this.internTest.name;
    }

    runTest() {
      try {
        var _this2 = this;

        var _temp2 = _finallyRethrows(function () {
          return Promise.resolve(_this2.setup()).then(function () {
            return Promise.resolve(_this2.beforeTest()).then(function () {
              return Promise.resolve(_this2.test()).then(function () {
                return Promise.resolve(_this2.afterTest()).then(function () {});
              });
            });
          });
        }, function (_wasThrown, _result) {
          return Promise.resolve(_this2.teardown()).then(function () {
            if (_wasThrown) throw _result;
            return _result;
          });
        });

        return Promise.resolve(_temp2 && _temp2.then ? _temp2.then(function () {}) : void 0);
      } catch (e) {
        return Promise.reject(e);
      }
    }

    get assert() {
      return intern.getPlugin('chai').assert;
    }

    get nextBeat() {
      return this.sleep(100);
    }

    sleep(ms) {
      try {
        return Promise.resolve(new Promise(resolve => setTimeout(resolve, ms)));
      } catch (e) {
        return Promise.reject(e);
      }
    }

    setup() {
      return Promise.resolve();
    }

    beforeTest() {
      return Promise.resolve();
    }

    get test() {
      var method = this["test " + this.testName];

      if (method != null && typeof method == 'function') {
        return method;
      } else {
        throw new Error("No such test \"" + this.testName + "\"");
      }
    }

    afterTest() {
      return Promise.resolve();
    }

    teardown() {
      return Promise.resolve();
    }

  }

  class FunctionalTestCase extends InternTestCase {
    get remote() {
      return this.internTest.remote;
    }

    goToLocation(location) {
      try {
        var _this2 = this;

        var processedLocation = location.match(/^\//) ? location.slice(1) : location;
        return Promise.resolve(_this2.remote.get(processedLocation));
      } catch (e) {
        return Promise.reject(e);
      }
    }

    goBack() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.remote.goBack());
      } catch (e) {
        return Promise.reject(e);
      }
    }

    goForward() {
      try {
        var _this6 = this;

        return Promise.resolve(_this6.remote.goForward());
      } catch (e) {
        return Promise.reject(e);
      }
    }

    reload() {
      try {
        var _this8 = this;

        return Promise.resolve(_this8.evaluate(() => location.reload())).then(function () {
          return _this8.nextBeat;
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    hasSelector(selector) {
      try {
        var _this10 = this;

        return Promise.resolve(_this10.remote.findAllByCssSelector(selector)).then(function (_this9$remote$findAll) {
          return _this9$remote$findAll.length > 0;
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    querySelector(selector) {
      try {
        var _this12 = this;

        return Promise.resolve(_this12.remote.findByCssSelector(selector));
      } catch (e) {
        return Promise.reject(e);
      }
    }

    clickSelector(selector) {
      try {
        var _this14 = this;

        return Promise.resolve(_this14.remote.findByCssSelector(selector).click());
      } catch (e) {
        return Promise.reject(e);
      }
    }

    scrollToSelector(selector) {
      try {
        var _this16 = this;

        return Promise.resolve(_this16.remote.findByCssSelector(selector)).then(function (element) {
          return _this16.evaluate(element => element.scrollIntoView(), element);
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    outerHTMLForSelector(selector) {
      try {
        var _this18 = this;

        return Promise.resolve(_this18.remote.findByCssSelector(selector)).then(function (element) {
          return _this18.evaluate(element => element.outerHTML, element);
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    innerHTMLForSelector(selector) {
      try {
        var _this20 = this;

        return Promise.resolve(_this20.remote.findAllByCssSelector(selector)).then(function (element) {
          return _this20.evaluate(element => element.innerHTML, element);
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    attributeForSelector(selector, attributeName) {
      try {
        var _this22 = this;

        return Promise.resolve(_this22.querySelector(selector)).then(function (element) {
          return Promise.resolve(element.getAttribute(attributeName));
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    propertyForSelector(selector, attributeName) {
      try {
        var _this24 = this;

        return Promise.resolve(_this24.querySelector(selector)).then(function (element) {
          return Promise.resolve(element.getProperty(attributeName));
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    get scrollPosition() {
      return this.evaluate(() => ({
        x: window.scrollX,
        y: window.scrollY
      }));
    }

    isScrolledToSelector(selector) {
      try {
        var _this26 = this;

        return Promise.resolve(_this26.scrollPosition).then(function (_ref) {
          var {
            y: pageY
          } = _ref;
          return Promise.resolve(_this26.remote.findByCssSelector(selector).getPosition()).then(function (_ref2) {
            var {
              y: elementY
            } = _ref2;
            var offset = pageY - elementY;
            return Math.abs(offset) < 2;
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    evaluate(callback) {
      try {
        var _this28 = this,
            _arguments2 = arguments;

        return Promise.resolve(_this28.remote.execute(callback, [].slice.call(_arguments2, 1)));
      } catch (e) {
        return Promise.reject(e);
      }
    }

    get head() {
      return this.evaluate(() => document.head);
    }

    get body() {
      return this.evaluate(() => document.body);
    }

    get location() {
      return this.evaluate(() => location.toString());
    }

    get origin() {
      return this.evaluate(() => location.origin.toString());
    }

    get pathname() {
      return this.evaluate(() => location.pathname);
    }

    get search() {
      return this.evaluate(() => location.search);
    }

    get searchParams() {
      return this.evaluate(() => location.search).then(search => new URLSearchParams(search));
    }

    getSearchParam(key) {
      try {
        var _this30 = this;

        return Promise.resolve(_this30.searchParams).then(function (_this29$searchParams) {
          return _this29$searchParams.get(key) || '';
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    getAllSearchParams(key) {
      try {
        var _this32 = this;

        return Promise.resolve(_this32.searchParams).then(function (_this31$searchParams) {
          return _this31$searchParams.getAll(key) || [];
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

    get hash() {
      return this.evaluate(() => location.hash);
    }

  }

  class DomTests extends FunctionalTestCase {
    setup() {
      try {
        var _this2 = this;

        return Promise.resolve(_this2.goToLocation('/src/examples/advanced/dom.html')).then(function () {});
      } catch (e) {
        return Promise.reject(e);
      }
    }

    'test dom open and close'() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.nextBeat).then(function () {
          return Promise.resolve(_this4.querySelector('#menu')).then(function (dom) {
            _this4.assert.exists(dom);

            var _this3$assert = _this4.assert,
                _isNull = _this3$assert.isNull;
            return Promise.resolve(dom.getAttribute('open')).then(function (_dom$getAttribute) {
              _isNull.call(_this3$assert, _dom$getAttribute);

              return Promise.resolve(_this4.clickSelector('button#btn_open')).then(function () {
                var _this3$assert2 = _this4.assert,
                    _isNotNull = _this3$assert2.isNotNull;
                return Promise.resolve(dom.getAttribute('open')).then(function (_dom$getAttribute2) {
                  _isNotNull.call(_this3$assert2, _dom$getAttribute2);

                  return Promise.resolve(_this4.clickSelector('button#btn_close')).then(function () {
                    var _this3$assert3 = _this4.assert,
                        _isNull2 = _this3$assert3.isNull;
                    return Promise.resolve(dom.getAttribute('open')).then(function (_dom$getAttribute3) {
                      _isNull2.call(_this3$assert3, _dom$getAttribute3);

                      return Promise.resolve(_this4.clickSelector('button#btn_toggle')).then(function () {
                        var _this3$assert4 = _this4.assert,
                            _isNotNull2 = _this3$assert4.isNotNull;
                        return Promise.resolve(dom.getAttribute('open')).then(function (_dom$getAttribute4) {
                          _isNotNull2.call(_this3$assert4, _dom$getAttribute4);

                          return Promise.resolve(_this4.clickSelector('button#btn_toggle')).then(function () {
                            var _this3$assert5 = _this4.assert,
                                _isNull3 = _this3$assert5.isNull;
                            return Promise.resolve(dom.getAttribute('open')).then(function (_dom$getAttribute5) {
                              _isNull3.call(_this3$assert5, _dom$getAttribute5);
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

  }
  DomTests.registerSuite();

  class HelloTests extends FunctionalTestCase {
    setup() {
      try {
        var _this2 = this;

        return Promise.resolve(_this2.goToLocation('/src/examples/basic/hello.html')).then(function () {});
      } catch (e) {
        return Promise.reject(e);
      }
    }

    'test hello#world'() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.nextBeat).then(function () {
          return Promise.resolve(_this4.querySelector('input[data-hello-target~=name]')).then(function (input) {
            _this4.assert.exists(input);

            var _this3$assert = _this4.assert,
                _equal = _this3$assert.equal;
            return Promise.resolve(input.getProperty('value')).then(function (_input$getProperty) {
              _equal.call(_this3$assert, _input$getProperty, 'hello#world method invoked!');
            });
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

  }
  HelloTests.registerSuite();

  class HelloWithModuleTests extends FunctionalTestCase {
    setup() {
      try {
        var _this2 = this;

        return Promise.resolve(_this2.goToLocation('/src/examples/basic/hello-with-module.html')).then(function () {});
      } catch (e) {
        return Promise.reject(e);
      }
    }

    'test hello with module#world'() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.sleep(500)).then(function () {
          return Promise.resolve(_this4.querySelector('input[data-hello-with-module-target~=name]')).then(function (input) {
            _this4.assert.exists(input);

            var _this3$assert = _this4.assert,
                _equal = _this3$assert.equal;
            return Promise.resolve(input.getProperty('value')).then(function (_input$getProperty) {
              _equal.call(_this3$assert, _input$getProperty, 'hello-with-module#world method invoked!');
            });
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

  }
  HelloWithModuleTests.registerSuite();

  class ModalTests extends FunctionalTestCase {
    setup() {
      try {
        var _this2 = this;

        return Promise.resolve(_this2.goToLocation('/src/examples/advanced/modal.html')).then(function () {});
      } catch (e) {
        return Promise.reject(e);
      }
    }

    'test modal open and close'() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.sleep(500)).then(function () {
          return Promise.resolve(_this4.querySelector('[data-controller~=modal] .modal-title')).then(function (modalTitle) {
            _this4.assert.exists(modalTitle);

            var _this3$assert = _this4.assert,
                _equal = _this3$assert.equal;
            return Promise.resolve(modalTitle.getVisibleText()).then(function (_modalTitle$getVisibl) {
              _equal.call(_this3$assert, _modalTitle$getVisibl, 'Modal title');

              return Promise.resolve(_this4.clickSelector('[data-controller~=modal] button.btn-close')).then(function () {
                return Promise.resolve(_this4.sleep(350)).then(function () {
                  var _this3$assert2 = _this4.assert,
                      _notEqual = _this3$assert2.notEqual;
                  return Promise.resolve(modalTitle.getVisibleText()).then(function (_modalTitle$getVisibl2) {
                    _notEqual.call(_this3$assert2, _modalTitle$getVisibl2, 'Modal title');
                  });
                });
              });
            });
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

  }
  ModalTests.registerSuite();

  class ModalWithTurboFrameTests extends FunctionalTestCase {
    setup() {
      try {
        var _this2 = this;

        return Promise.resolve(_this2.goToLocation('/src/examples/advanced/modal-with-turbo-frame.html')).then(function () {});
      } catch (e) {
        return Promise.reject(e);
      }
    }

    'test modal open and close it with turbo frame'() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.sleep(500)).then(function () {
          return Promise.resolve(_this4.querySelector('[data-controller~=modal] .modal-title')).then(function (modalTitle) {
            _this4.assert.exists(modalTitle);

            var _this3$assert = _this4.assert,
                _equal = _this3$assert.equal;
            return Promise.resolve(modalTitle.getVisibleText()).then(function (_modalTitle$getVisibl) {
              _equal.call(_this3$assert, _modalTitle$getVisibl, 'Modal title');

              return Promise.resolve(_this4.clickSelector('[data-controller~=modal] [data-turbo-frame].btn-primary')).then(function () {
                return Promise.resolve(_this4.sleep(350)).then(function () {
                  var _this3$assert2 = _this4.assert,
                      _notEqual = _this3$assert2.notEqual;
                  return Promise.resolve(modalTitle.getVisibleText()).then(function (_modalTitle$getVisibl2) {
                    _notEqual.call(_this3$assert2, _modalTitle$getVisibl2, 'Modal title');
                  });
                });
              });
            });
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

  }
  ModalWithTurboFrameTests.registerSuite();

  class ModalWithTurboStreamTests extends FunctionalTestCase {
    setup() {
      try {
        var _this2 = this;

        return Promise.resolve(_this2.goToLocation('/src/examples/advanced/modal-with-turbo-stream.html')).then(function () {});
      } catch (e) {
        return Promise.reject(e);
      }
    }

    'test modal open and close it with turbo stream'() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.sleep(500)).then(function () {
          return Promise.resolve(_this4.querySelector('[data-controller~=modal] .modal-title')).then(function (modalTitle) {
            _this4.assert.exists(modalTitle);

            var _this3$assert = _this4.assert,
                _equal = _this3$assert.equal;
            return Promise.resolve(modalTitle.getVisibleText()).then(function (_modalTitle$getVisibl) {
              _equal.call(_this3$assert, _modalTitle$getVisibl, 'Modal title');

              return Promise.resolve(_this4.clickSelector('[data-controller~=modal] form > button.btn-primary')).then(function () {
                return Promise.resolve(_this4.sleep(350)).then(function () {
                  var _this3$assert2 = _this4.assert,
                      _notEqual = _this3$assert2.notEqual;
                  return Promise.resolve(modalTitle.getVisibleText()).then(function (_modalTitle$getVisibl2) {
                    _notEqual.call(_this3$assert2, _modalTitle$getVisibl2, 'Modal title');
                  });
                });
              });
            });
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

  }
  ModalWithTurboStreamTests.registerSuite();

  class SelectboxTests extends FunctionalTestCase {
    setup() {
      try {
        var _this2 = this;

        return Promise.resolve(_this2.goToLocation('/src/examples/advanced/selectbox.html')).then(function () {});
      } catch (e) {
        return Promise.reject(e);
      }
    }

    'test selectbox open and close'() {
      try {
        var _this4 = this;

        return Promise.resolve(_this4.nextBeat).then(function () {
          return Promise.resolve(_this4.querySelector('[data-controller="selectbox"]')).then(function (controller) {
            _this4.assert.exists(controller);

            return Promise.resolve(controller.getAttribute('data-ssid')).then(function (selectboxUid) {
              _this4.assert.isString(selectboxUid);

              return Promise.resolve(_this4.querySelector("." + selectboxUid)).then(function (selectbox) {
                _this4.assert.exists(selectbox);

                var _this3$assert = _this4.assert,
                    _notInclude = _this3$assert.notInclude;
                return Promise.resolve(selectbox.getVisibleText()).then(function (_selectbox$getVisible) {
                  _notInclude.call(_this3$assert, _selectbox$getVisible, 'One\nTwo\nThree');

                  return Promise.resolve(_this4.clickSelector('button#btn_open')).then(function () {
                    return Promise.resolve(_this4.sleep(250)).then(function () {
                      var _this3$assert2 = _this4.assert,
                          _include = _this3$assert2.include;
                      return Promise.resolve(selectbox.getVisibleText()).then(function (_selectbox$getVisible2) {
                        _include.call(_this3$assert2, _selectbox$getVisible2, 'One\nTwo\nThree');

                        return Promise.resolve(_this4.clickSelector('button#btn_close')).then(function () {
                          return Promise.resolve(_this4.sleep(250)).then(function () {
                            var _this3$assert3 = _this4.assert,
                                _notInclude2 = _this3$assert3.notInclude;
                            return Promise.resolve(selectbox.getVisibleText()).then(function (_selectbox$getVisible3) {
                              _notInclude2.call(_this3$assert3, _selectbox$getVisible3, 'One\nTwo\nThree');

                              return Promise.resolve(_this4.clickSelector('button#btn_open_search')).then(function () {
                                return Promise.resolve(_this4.sleep(250)).then(function () {
                                  var _this3$assert4 = _this4.assert,
                                      _include2 = _this3$assert4.include;
                                  return Promise.resolve(selectbox.getVisibleText()).then(function (_selectbox$getVisible4) {
                                    _include2.call(_this3$assert4, _selectbox$getVisible4, 'Two');

                                    var _this3$assert5 = _this4.assert,
                                        _notInclude3 = _this3$assert5.notInclude;
                                    return Promise.resolve(selectbox.getVisibleText()).then(function (_selectbox$getVisible5) {
                                      _notInclude3.call(_this3$assert5, _selectbox$getVisible5, 'One\nTwo\nThree');
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }

  }
  SelectboxTests.registerSuite();

  exports.DomTests = DomTests;
  exports.HelloTests = HelloTests;
  exports.HelloWithModuleTests = HelloWithModuleTests;
  exports.ModalTests = ModalTests;
  exports.ModalWithTurboFrameTests = ModalWithTurboFrameTests;
  exports.ModalWithTurboStreamTests = ModalWithTurboStreamTests;
  exports.SelectboxTests = SelectboxTests;

}));
//# sourceMappingURL=functional.js.map
