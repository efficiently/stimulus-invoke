module.exports = {
  extends: [
    'eslint:recommended',
    'google',
  ],
  parser: '@babel/eslint-parser',
  parserOptions: {
    'requireConfigFile': false,
    'babelOptions': {
      'configFile': './babel.config.js',
    },
  },
  rules: {
    'indent': ['error', 2, { 'SwitchCase': 1 }],
    'no-undef': 0,
    'max-len': ['warn', { 'code': 120 }],
    'object-curly-spacing': ['error', 'always', { 'objectsInObjects': true }],
    'operator-linebreak': ['error', 'after', { 'overrides': { '?': 'before', ':': 'before' } }],
    'quotes': ['warn', 'single'],
    'require-jsdoc': 0,
    'semi': ['warn', 'never'],
    'comma-dangle': ['warn', 'always-multiline'],
    '@babel/semi': 0,
  },
}
