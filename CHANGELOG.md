# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-12-15
### Changed
- Update some JavaScript dependencies

## [1.0.0-beta.3] - 2021-12-06
### Added
- Support Stimulus 3.0 [action parameters](https://stimulus.hotwired.dev/reference/actions#action-parameters) system
- Bump Turbo to version [7.1.0](https://github.com/hotwired/turbo/tree/v7.1.0) in tests

### Fixed
- Fix installation instructions with ECMAScript module in README.md
- Fix documentation in README

## [1.0.0-beta.2] - 2021-10-07
### Changed
- Bump [Stimulus](https://github.com/hotwired/stimulus/tree/v3.0.1) dependency to 3.0.1
- Default to toggle event on details HTML element ([@hotwired/stimulus#464](https://github.com/hotwired/stimulus/pull/464))

## [1.0.0-beta.1] - 2021-10-05
### Added
- Add a [CHANGELOG](CHANGELOG.md), this file itself!

### Changed
- Rename NPM command `packaging` to `fresh-build`

## [1.0.0-alpha.1] - 2021-10-05
### Fixed
- Fix dist files build, with the `packaging` NPM command instead of the deprecated `prepublish`

## [1.0.0-alpha] - 2021-10-05
### Added
- [Stimulus 3.0](https://github.com/hotwired/stimulus/tree/v3.0.0) support

### Removed
- **Breaking Change**: Drop compatibility with [Stimulus 2.0](https://github.com/hotwired/stimulus/tree/v2.0.0), you should use the [0.1](https://gitlab.com/efficiently/stimulus-invoke/-/tree/0-1-stable) branch if you can't upgrade to the latest version of Stimulus
- Drop `dist/stimulus-invoke.umd.min.js` file, you should use a <abbr title="Content Delivery Network">CDN</abbr> with minification instead (e.g. https://cdn.jsdelivr.net/npm/stimulus-invoke/dist/stimulus-invoke.umd.min.js)

## [0.1.0] - 2021-09-27
### Added
- First stable release with [Stimulus 2.0](https://github.com/hotwired/stimulus/tree/v2.0.0) support

[Unreleased]: https://gitlab.com/efficiently/stimulus-invoke/compare/v1.0.0...main
[1.0.0]: https://gitlab.com/efficiently/stimulus-invoke/-/tags/v1.0.0
[1.0.0-beta.3]: https://gitlab.com/efficiently/stimulus-invoke/-/tags/v1.0.0-beta.3
[1.0.0-beta.2]: https://gitlab.com/efficiently/stimulus-invoke/-/tags/v1.0.0-beta.2
[1.0.0-beta.1]: https://gitlab.com/efficiently/stimulus-invoke/-/tags/v1.0.0-beta.1
[1.0.0-alpha.1]: https://gitlab.com/efficiently/stimulus-invoke/-/tags/v1.0.0-alpha.1
[1.0.0-alpha]: https://gitlab.com/efficiently/stimulus-invoke/-/tags/v1.0.0-alpha
[0.1.0]: https://gitlab.com/efficiently/stimulus-invoke/-/tags/v0.1.0
