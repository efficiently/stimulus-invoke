FROM node:lts-buster

RUN set -e; \
    apt-get --quiet update --yes; \
    apt-get --quiet install bash curl git psmisc default-jre chromium chromium-driver firefox-esr --yes; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*; \
    curl -fL https://selenium-release.storage.googleapis.com/3.141/selenium-server-standalone-3.141.59.jar -o /usr/local/bin/selenium-server-standalone.jar; \
    echo 'java -jar /usr/local/bin/selenium-server-standalone.jar "$@"' > /usr/local/bin/selenium-server; \
    chmod +x /usr/local/bin/selenium-server; \
    curl -fL https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz -o geckodriver-linux64.tar.gz; \
    tar -xvzf geckodriver-linux64.tar.gz -C /usr/local/bin/; \
    rm -f geckodriver-linux64.tar.gz

