import { camelCase, toggleAttributeFrom } from './utils'

export class StimulusInvokeElement extends HTMLElement {
  static config = {
    tagName: 'stimulus-invoke',
    controllerName: 'invoke',
    controllerTagName: 'span',
  }

  static get observedAttributes() {
    return [
      'disabled',
      'action',
      'target',
      'source',
      'params',
      'on',
      'keep',
      'once',
      'data-controller-name',
      'data-controller-action',
      'id',
    ]
  }

  static define() {
    customElements.define(this.config.tagName, this)
  }

  constructor() {
    // Always call super first in constructor
    super()

    this.config = { ...this.constructor.config }

    this.config.controllerApplyEvent = `${this.config.controllerName}:connected`
    this.config.controllerRemoveEvent = `${this.config.controllerName}:applied`

    this.config.controllerApplyAction = `${this.config.controllerApplyEvent}->${this.config.controllerName}#apply`
    this.config.controllerRemoveAction = `${this.config.controllerRemoveEvent}->${this.config.controllerName}#remove`

    this._controller = null

    if (this.hasAttribute('disabled')) {
      return // short circuit
    }

    this.render()
  }

  render() {
    if (this.parentElement && !this.hasAttribute('action')) {
      this.handleError('missing "action" attribute')
    }

    // Create (nested) Stimulus controller
    this._controller = document.createElement(this._controllerTagName)
    if (this.innerHTML) {
      this._controller.innerHTML = this.innerHTML
      this.innerHTML = null
    }

    this._dataControllerNameChanged = this.dataControllerName
    this._dataControllerActionChanged = this.dataControllerAction

    this.append(this._controller)

    this._handleDisconnectedElement()
  }

  get controller() {
    return this._controller?.parentElement ? this._controller : null
  }

  set controller(val) {
    if (val && !(val instanceof Element)) {
      this.handleError('controller property must be an HTML Element')
    }
    this._controller = val
  }

  get dataControllerName() {
    return this.getAttribute('data-controller-name') || this.config.controllerName
  }

  get dataControllerAction() {
    const controllerApplyAction = this.hasAttribute('on')
      ? this.getAttribute('on')
        .split(' ')
        .map((on) => `${on}->${this.config.controllerName}#apply`)
        .join(' ')
      : this.config.controllerApplyAction
    // If the "keep" attribute is set, the invoke controller isn't removed
    // after it applied delegated action
    const controllerDefaultAction = this.getAttribute('data-controller-action') || controllerApplyAction

    const controllerAction = this.hasAttribute('keep')
      ? controllerDefaultAction
      : `${controllerDefaultAction} ${this.config.controllerRemoveAction}`

    return controllerAction
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue !== newValue) {
      this[`_${camelCase(name)}Changed`] = newValue
    }
  }

  handleError(message) {
    throw new Error(`${this._description}: ${message}`)
  }

  // Private

  _handleDisconnectedElement() {
    if (this._controller) {
      const eventName = `${this.dataControllerName}:disconnected`
      const listener = () => {
        this.remove()
      }

      // Remove previous event listeners, if any
      this._controller.removeEventListener(eventName, listener)

      this._controller.addEventListener(eventName, listener, { once: true })
    }
  }

  get _controllerTagName() {
    return this.getAttribute('tag') || this.config.controllerTagName
  }

  set _disabledChanged(val) {
    if (this.hasAttribute('disabled')) {
      this._controller?.removeAttribute('data-action')
    }
  }

  set _actionChanged(val) {
    toggleAttributeFrom(this.controller, `data-${this.dataControllerName}-action-value`, val)
  }

  set _targetChanged(val) {
    toggleAttributeFrom(this.controller, `data-${this.dataControllerName}-target-value`, val)
  }

  set _sourceChanged(val) {
    toggleAttributeFrom(this.controller, `data-${this.dataControllerName}-source-value`, val)
  }

  set _paramsChanged(val) {
    toggleAttributeFrom(this.controller, `data-${this.dataControllerName}-params-value`, val)
  }

  set _onChanged(val) {
    if (!this.hasAttribute('once') && !this.hasAttribute('keep')) {
      this.setAttribute('keep', '')
    }
    this._dataControllerActionChanged = this.dataControllerAction
  }

  set _idChanged(val) {
    toggleAttributeFrom(this.controller, 'id', val ? `${val}_controller` : val)
  }

  set _keepChanged(val) {
    if (this.hasAttribute('keep')) {
      if (this.hasAttribute('once')) {
        this.removeAttribute('once')
      }
    }

    this._dataControllerActionChanged = this.dataControllerAction
  }

  set _onceChanged(val) {
    if (this.hasAttribute('once')) {
      if (this.hasAttribute('keep')) {
        this.removeAttribute('keep')
      }
    }
  }

  set _dataControllerNameChanged(val) {
    if (this._controller) {
      this._controller.setAttribute('data-controller', this.dataControllerName)
    }
  }

  set _dataControllerActionChanged(val) {
    if (this._controller) {
      this._controller.setAttribute('data-action', this.dataControllerAction)
    }
  }

  get _description() {
    return (this.outerHTML.match(/<[^>]+>/) ?? [])[0] ?? `<${this.constructor.tagName}>`
  }
}
