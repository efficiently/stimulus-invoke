import { StimulusInvokeElement } from './element'
import { camelCase } from './utils'
import { getDefaultEventNameForElement, parseActionDescriptorString } from './utils'

export class StimulusInvokeElementReactive extends StimulusInvokeElement {
  refresh() {
    this.render()
    const observedAttributes = this.constructor.observedAttributes || []
    observedAttributes.forEach((attribute) => {
      this[`_${camelCase(attribute)}Changed`] = this[`${camelCase(attribute)}`]
    })
  }

  get disabled() {
    return this.hasAttribute('disabled')
  }

  set disabled(val) {
    if (val !== null && val !== undefined && val !== false) {
      this.setAttribute('disabled', '')
    } else {
      this.removeAttribute('disabled')
    }
  }

  get action() {
    return this.getAttribute('action')
  }

  set action(val) {
    if (val) {
      this.setAttribute('action', val)
    } else {
      this.removeAttribute('action')
    }
  }

  get target() {
    return this.getAttribute('target')
  }

  set target(val) {
    if (val) {
      this.setAttribute('target', val)
    } else {
      this.removeAttribute('target')
    }
  }

  get source() {
    return this.getAttribute('source')
  }

  set source(val) {
    if (val) {
      this.setAttribute('source', val)
    } else {
      this.removeAttribute('source')
    }
  }

  get on() {
    return this.getAttribute('on')
  }

  set on(val) {
    if (val) {
      this.setAttribute('on', val)
    } else {
      this.removeAttribute('on')
    }
  }

  get params() {
    return this.getAttribute('params')
  }

  set params(val) {
    if (val) {
      this.setAttribute('params', val)
    } else {
      this.removeAttribute('params')
    }
  }

  get keep() {
    return this.hasAttribute('keep')
  }

  set keep(val) {
    if (val !== null && val !== undefined && val !== false) {
      this.setAttribute('keep', '')
    } else {
      this.removeAttribute('keep')
    }
  }

  get once() {
    return this.hasAttribute('once')
  }

  set once(val) {
    if (val !== null && val !== undefined && val !== false) {
      this.setAttribute('once', '')
    } else {
      this.removeAttribute('once')
    }
  }

  // Dispatch the first invoke event, if any,
  // to invoke the action delegated by the StimulusInvokeController
  // Useful when an attribute or property of the StimulusInvokeElement changed
  apply() {
    if (!this.disabled) {
      if (!this.parentElement) {
        if (!this.hasAttribute('action')) {
          this.handleError('missing "action" attribute')
        }
        document?.body?.append(this)
        if (!this.on) {
          return // short circuit
        }
      }

      if (this.controller) {
        const actionDescriptor = parseActionDescriptorString(this.dataControllerAction)
        const eventName = actionDescriptor.eventName || getDefaultEventNameForElement(this.controller)
        if (eventName) {
          const eventTarget = new Event(eventName, {
            bubbles: true,
            cancelable: true,
            composed: true,
          })
          this.controller.dispatchEvent(eventTarget)
        }
      }
    }
  }

  destroy() {
    try {
      this._controller?.remove()
      this.remove()
    } catch {
      // Do nothing
    }
  }

  // Private

  // Override parent method to refresh the element
  // when the disabled attribute is updated programmatically
  set _disabledChanged(val) {
    if (this.hasAttribute('disabled')) {
      this._controller?.removeAttribute('data-action')
    } else {
      if (this.controller) {
        this._controller.setAttribute('data-action', this.dataControllerAction)
      } else {
        this.refresh()
      }
    }
  }
}
