import { Controller } from '@hotwired/stimulus'
import { getDefaultEventNameForElement, parseActionDescriptorString } from './utils'
import { removeAttributeValueFrom, findElement, findElements, isElementPropertyWritable } from './utils'

export class StimulusInvokeController extends Controller {
  static targets = ['source']

  static values = {
    action: String,
    target: String,
    source: String,
    params: Array,
  }

  static config = {
    domAccess: false,
    domIdentifier: 'DOM',
    propertyWritable: false,
  }

  initialize() {
    // Always call super.initialize()
    super.initialize()

    const getControllerForElementAndIdentifier = this?.application?.getControllerForElementAndIdentifier?.bind(
      this?.application
    )
    this._getControllerForElementAndIdentifier = getControllerForElementAndIdentifier

    if (
      !this._getControllerForElementAndIdentifier ||
      typeof this._getControllerForElementAndIdentifier !== 'function'
    ) {
      this._error('Unable to find Stimulus controller(s) without the "getControllerForElementAndIdentifier()" method!')
    }
  }

  connect() {
    const customEvent = new CustomEvent(`${this.identifier}:connected`)
    this.element.dispatchEvent(customEvent)
  }

  disconnect() {
    const customEvent = new CustomEvent(`${this.identifier}:disconnected`)
    this.element.dispatchEvent(customEvent)
  }

  // Actions

  apply() {
    if (this.actionValue) {
      const actions = this.actionValue.split(' ')
      actions.forEach((action) => {
        this._invokeAction(action)
      })
    } else {
      // TODO: Raise an error? Otherwise if an actionValue has the :once option,
      // it'll raised an error, the second time it's called...
      // this._error(`"data-${this.identifier}-action-value" HTML attribute must be set!`)
      console.warn(`"data-${this.identifier}-action-value" HTML attribute should be set`)
    }

    const customEvent = new CustomEvent(`${this.identifier}:applied`)
    this.element.dispatchEvent(customEvent)
  }

  remove() {
    this.element.remove()

    const customEvent = new CustomEvent(`${this.identifier}:removed`)
    this.element.dispatchEvent(customEvent)
  }

  get sourceElement() {
    return this.hasSourceTarget ? this.sourceTarget : this.sourceValue ? findElement(this.sourceValue) : null
  }

  get domAccess() {
    return this.constructor.config.domAccess
  }

  get domIdentifier() {
    return this.constructor.config.domIdentifier
  }

  get propertyWritable() {
    return this.constructor.config.propertyWritable
  }

  // Private

  _error(message) {
    throw new Error(message)
  }

  // Borrowed from: https://github.com/hotwired/turbo/pull/113
  get _targetElements() {
    if (this.targetValue) {
      const elements = findElements(this.targetValue)
      if (elements?.length) {
        return elements
      }

      this._error(`Unable to find target with this selector: "${this.targetValue}"`)
    }

    return []
  }

  _getTargetElements(identifier) {
    if (this.targetValue) {
      return this._targetElements
    }

    const elements = [this.element.closest(`[data-controller~="${identifier}"]`)]
    if (elements?.length === 0) {
      const errorMessage = `Unable to find Stimulus controller(s) "${identifier}"!`
      this._error(errorMessage)
    }

    return elements
  }

  _handleEvent(eventTarget, eventName, listener) {
    const event = new Event(eventName, {
      bubbles: true,
      cancelable: true,
      composed: true,
    })

    // Remove previous event listeners, if any
    eventTarget.removeEventListener(eventName, listener)

    eventTarget.addEventListener(eventName, listener, { once: true })
    eventTarget.dispatchEvent(event)
  }

  _invokeAction(action) {
    const actionDescriptor = parseActionDescriptorString(action)
    const controllerEventTarget = actionDescriptor.eventTarget
    const controllerEventName = actionDescriptor.eventName
    const controllerEventOptions = actionDescriptor.eventOptions
    const controllerIdentifier = actionDescriptor.identifier || this._error('missing identifier')
    const controllerMethodName = actionDescriptor.methodName || this._error('missing method name')

    const controllerElements = this._getTargetElements(controllerIdentifier)
    controllerElements.forEach((controllerElement) => {
      if (!(controllerElement instanceof Element)) {
        this._error(`Unable to find Stimulus controller "${controllerIdentifier}"!`)
      }

      let currentControllerEventTarget = controllerEventTarget
      let currentControllerEventName = controllerEventName

      if (this.sourceElement) {
        currentControllerEventTarget = currentControllerEventTarget || this.sourceElement
        currentControllerEventName = currentControllerEventName || getDefaultEventNameForElement(controllerElement)
      }

      // Handle target which is inside a Stimulus controller
      if (this.targetValue && !controllerElement.matches(`[data-controller~="${controllerIdentifier}"]`)) {
        currentControllerEventTarget = currentControllerEventTarget || controllerElement

        currentControllerEventName = currentControllerEventName || getDefaultEventNameForElement(controllerElement)

        if ((this.domAccess && controllerIdentifier !== this.domIdentifier) || !this.domAccess) {
          controllerElement = controllerElement.closest(`[data-controller~="${controllerIdentifier}"]`)
        }
        if (!controllerElement || !(controllerElement instanceof Element)) {
          this._error(
            `Unable to find a parent Stimulus controller "${controllerIdentifier}", ` +
              `for the target with this selector: "${this.targetValue}"!`
          )
        }
      }

      currentControllerEventTarget = currentControllerEventTarget || this.element
      currentControllerEventName = currentControllerEventName || getDefaultEventNameForElement(this.element)

      const controller =
        (this.domAccess && controllerIdentifier === this.domIdentifier ? controllerElement : null) ||
        this._getControllerForElementAndIdentifier(controllerElement, controllerIdentifier)

      if (!controller) {
        this._error(`Unable to find a Stimulus controller instance for "${controllerIdentifier}"!`)
      }

      if (!controller[controllerMethodName]) {
        this._error(
          `Action "${action}" references undefined method${
            this.propertyWritable ? ' or property' : ''
          } "${controllerMethodName}"`
        )
      }

      if (typeof controller[controllerMethodName] !== 'function') {
        if (!this.propertyWritable) {
          this._error(
            `Action "${action}" doesn't reference a method named "${controllerMethodName}", maybe it's a property?`
          )
        }

        if (!isElementPropertyWritable(controller, controllerMethodName)) {
          this._error(`Action "${action}" references undefined or read only property "${controllerMethodName}"`)
        }
      }

      // Without a currentControllerEventName value, it returns something like:
      //   'current-identifier:controller-identifier:methodName'
      //  e.g.
      //   'invoke:bootstrap-modal:show'
      //
      // With a currentControllerEventName value, it returns something like:
      //  'current-identifier:controller-identifier:methodName.eventName'
      //  e.g.
      //  'invoke:bootstrap-modal:show.click'
      const controllerEventType = `${this.identifier}:${controllerIdentifier}:${controllerMethodName}${
        currentControllerEventName ? `.${currentControllerEventName}` : ''
      }`

      const listener = (event) => {
        const [param] = this.paramsValue

        if (this.paramsValue.length === 1 &&
          (
            !this.domAccess || controllerIdentifier !== this.domIdentifier
          ) &&
          event instanceof Event &&
          typeof param === 'object' &&
          !Array.isArray(param) &&
          param !== null
        ) {
          // Support Stimulus 3.0 action parameters system
          // See: https://stimulus.hotwired.dev/reference/actions#action-parameters
          event['params'] = param
        }
        // Keep original Stimulus Invoke params system,
        // but it might be deprecated on the next minor release,
        // only when dealing with a real Stimulus controller of course
        const params = [event, ...this.paramsValue]
        if (this.domAccess && controllerIdentifier === this.domIdentifier) {
          params.shift() // event isn't used for DOM manipulation
        }
        if (typeof controller[controllerMethodName] === 'function') {
          controller[controllerMethodName](...params)
        } else {
          if (param === undefined) {
            console.warn(`No value given to set the property "${controllerMethodName}" in action "${action}"`)
          } else {
            controller[controllerMethodName] = param
          }
        }
      }

      this._handleEvent(currentControllerEventTarget, controllerEventType, listener)
    })

    if (controllerEventOptions?.once) {
      removeAttributeValueFrom(this.element, `data-${this.identifier}-action-value`, action)
    }
  }
}
