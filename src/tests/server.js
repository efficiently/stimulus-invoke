const express = require('express')
const path = require('path')
const app = express()

app.use((request, response, next) => {
  if (request.accepts(['text/html', 'application/xhtml+xml'])) {
    next()
  } else {
    response.sendStatus(422)
  }
})

app.use('/', express.static('src/examples'))
app.use('/dist', express.static('dist'))

app.get('/src/examples', (request, response) => {
  response.redirect('/')
})

app.post(['/turbo/streams/modal-hide.html', '/src/examples/turbo/streams/modal-hide.html'], (request, response) => {
  if (acceptsStreams(request)) {
    response.type('text/vnd.turbo-stream.html; charset=utf-8')
    response.sendFile(path.join(__dirname, '../examples/turbo/streams/modal-hide.html'))
  } else {
    response.sendStatus(422)
  }
})

function acceptsStreams(request) {
  return !!request.accepts('text/vnd.turbo-stream.html')
}

module.exports = app
