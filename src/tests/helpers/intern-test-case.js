export class InternTestCase {
  static registerSuite() {
    return intern.getInterface('object').registerSuite(this.name, { tests: this.tests })
  }

  static get tests() {
    return this.testNames.reduce((tests, testName) => {
      return {
        ...tests,
        [testName]: (internTest) => this.runTest(internTest),
      }
    }, {})
  }

  static get testNames() {
    return this.testKeys.map((key) => key.slice(5))
  }

  static get testKeys() {
    return Object.getOwnPropertyNames(this.prototype).filter((key) => key.match(/^test /))
  }

  static runTest(internTest) {
    const testCase = new this(internTest)
    return testCase.runTest()
  }

  constructor(internTest) {
    this.internTest = internTest
  }

  get testName() {
    return this.internTest.name
  }

  async runTest() {
    try {
      await this.setup()
      await this.beforeTest()
      await this.test()
      await this.afterTest()
    } finally {
      await this.teardown()
    }
  }

  get assert() {
    return intern.getPlugin('chai').assert
  }

  get nextBeat() {
    return this.sleep(100)
  }

  async sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms))
  }

  async setup() {}

  async beforeTest() {}

  get test() {
    const method = this[`test ${this.testName}`]
    if (method != null && typeof method == 'function') {
      return method
    } else {
      throw new Error(`No such test "${this.testName}"`)
    }
  }

  async afterTest() {}

  async teardown() {}
}
