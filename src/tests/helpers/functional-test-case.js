import { InternTestCase } from './intern-test-case'

export class FunctionalTestCase extends InternTestCase {
  get remote() {
    return this.internTest.remote
  }

  async goToLocation(location) {
    const processedLocation = location.match(/^\//) ? location.slice(1) : location
    return this.remote.get(processedLocation)
  }

  async goBack() {
    return this.remote.goBack()
  }

  async goForward() {
    return this.remote.goForward()
  }

  async reload() {
    await this.evaluate(() => location.reload())
    return this.nextBeat
  }

  async hasSelector(selector) {
    return (await this.remote.findAllByCssSelector(selector)).length > 0
  }

  async querySelector(selector) {
    return this.remote.findByCssSelector(selector)
  }

  async clickSelector(selector) {
    return this.remote.findByCssSelector(selector).click()
  }

  async scrollToSelector(selector) {
    const element = await this.remote.findByCssSelector(selector)
    return this.evaluate((element) => element.scrollIntoView(), element)
  }

  async outerHTMLForSelector(selector) {
    const element = await this.remote.findByCssSelector(selector)
    return this.evaluate((element) => element.outerHTML, element)
  }

  async innerHTMLForSelector(selector) {
    const element = await this.remote.findAllByCssSelector(selector)
    return this.evaluate((element) => element.innerHTML, element)
  }

  async attributeForSelector(selector, attributeName) {
    const element = await this.querySelector(selector)

    return await element.getAttribute(attributeName)
  }

  async propertyForSelector(selector, attributeName) {
    const element = await this.querySelector(selector)

    return await element.getProperty(attributeName)
  }

  get scrollPosition() {
    return this.evaluate(() => ({ x: window.scrollX, y: window.scrollY }))
  }

  async isScrolledToSelector(selector) {
    const { y: pageY } = await this.scrollPosition
    const { y: elementY } = await this.remote.findByCssSelector(selector).getPosition()
    const offset = pageY - elementY
    return Math.abs(offset) < 2
  }

  async evaluate(callback, ...args) {
    return await this.remote.execute(callback, args)
  }

  get head() {
    return this.evaluate(() => document.head)
  }

  get body() {
    return this.evaluate(() => document.body)
  }

  get location() {
    return this.evaluate(() => location.toString())
  }

  get origin() {
    return this.evaluate(() => location.origin.toString())
  }

  get pathname() {
    return this.evaluate(() => location.pathname)
  }

  get search() {
    return this.evaluate(() => location.search)
  }

  get searchParams() {
    return this.evaluate(() => location.search).then((search) => new URLSearchParams(search))
  }

  async getSearchParam(key) {
    return (await this.searchParams).get(key) || ''
  }

  async getAllSearchParams(key) {
    return (await this.searchParams).getAll(key) || []
  }

  get hash() {
    return this.evaluate(() => location.hash)
  }
}
