import { DOMTestCase } from '../helpers/dom-test-case'
import { Application } from '@hotwired/stimulus'
import { StimulusInvokeController } from '../../controller'
import { StimulusInvokeElementReactive } from '../../element-reactive'

export class ElementReactiveTests extends DOMTestCase {
  static beforeAllTest = StimulusInvokeElementReactive.define()

  async 'test create new'() {
    this.assert.doesNotThrow(() => {
      new StimulusInvokeElementReactive()
    })
  }

  async 'test apply() action'() {
    this.setupStimulusInvokeConfig()
    this.fixtureHTML = `<div><details id="menu">
    <summary>Menu</summary>
    Menu opened on DOM load, programmatically with the StimulusInvokeElementReactive class!
  </details></div>`
    await this.nextBeat

    this.assert.isNull(document.querySelector('stimulus-invoke > [data-controller="invoke"]'))
    const element = new StimulusInvokeElementReactive()

    const controller = element?.controller
    this.assert.exists(controller)
    this.assert.isTrue(controller.matches('[data-controller="invoke"]'))
    this.assert.equal(
      controller.getAttribute('data-action'),
      'invoke:connected->invoke#apply invoke:applied->invoke#remove'
    )

    element.action = 'DOM#click'
    this.assert.equal(element.action, 'DOM#click')
    this.assert.equal(controller.getAttribute('data-invoke-action-value'), 'DOM#click')

    element.target = '#menu > summary'
    this.assert.equal(element.target, '#menu > summary')
    this.assert.equal(controller.getAttribute('data-invoke-target-value'), '#menu > summary')

    this.assert.isFalse(element.keep)
    this.assert.isFalse(element.once)
    // 'keep' property is use here only to keep the <stimulus-invoke> element in the DOM,
    // for understanding and debugging purpose
    element.keep = true
    this.assert.isTrue(element.keep)
    this.assert.isFalse(element.once)
    this.assert.equal(controller.getAttribute('data-action'), 'invoke:connected->invoke#apply')

    this.assert.isNotTrue(this.find('#menu')?.hasAttribute('open'))

    this.assert.isNull(document.querySelector('stimulus-invoke > [data-controller="invoke"]'))
    element.apply()
    this.assert.exists(document.querySelector('stimulus-invoke > [data-controller="invoke"]'))

    await this.nextBeat
    this.assert.isTrue(this.find('#menu')?.hasAttribute('open'))
  }

  setupStimulusInvokeConfig() {
    (() => {
      const application = Application.start()

      // Allow to invoke any DOM Element methods,
      // use at your own risk, false by default
      StimulusInvokeController.config.domAccess = true

      // Register StimulusInvokeController
      application.register('invoke', StimulusInvokeController)
    })()
  }
}

ElementReactiveTests.registerSuite()
