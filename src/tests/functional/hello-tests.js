import { FunctionalTestCase } from '../helpers/functional-test-case'

export class HelloTests extends FunctionalTestCase {
  async setup() {
    await this.goToLocation('/src/examples/basic/hello.html')
  }

  async 'test hello#world'() {
    await this.nextBeat
    const input = await this.querySelector('input[data-hello-target~=name]')
    this.assert.exists(input)
    this.assert.equal(await input.getProperty('value'), 'hello#world method invoked!')
  }
}

HelloTests.registerSuite()
