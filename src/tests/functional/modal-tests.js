import { FunctionalTestCase } from '../helpers/functional-test-case'

export class ModalTests extends FunctionalTestCase {
  async setup() {
    await this.goToLocation('/src/examples/advanced/modal.html')
  }

  async 'test modal open and close'() {
    await this.sleep(500)
    const modalTitle = await this.querySelector('[data-controller~=modal] .modal-title')
    this.assert.exists(modalTitle)
    this.assert.equal(await modalTitle.getVisibleText(), 'Modal title')

    await this.clickSelector('[data-controller~=modal] button.btn-close')
    await this.sleep(350)
    this.assert.notEqual(await modalTitle.getVisibleText(), 'Modal title')
  }
}

ModalTests.registerSuite()
