import { FunctionalTestCase } from '../helpers/functional-test-case'

export class ModalWithTurboStreamTests extends FunctionalTestCase {
  async setup() {
    await this.goToLocation('/src/examples/advanced/modal-with-turbo-stream.html')
  }

  async 'test modal open and close it with turbo stream'() {
    await this.sleep(500)
    const modalTitle = await this.querySelector('[data-controller~=modal] .modal-title')
    this.assert.exists(modalTitle)
    this.assert.equal(await modalTitle.getVisibleText(), 'Modal title')

    await this.clickSelector('[data-controller~=modal] form > button.btn-primary')
    await this.sleep(350)
    this.assert.notEqual(await modalTitle.getVisibleText(), 'Modal title')
  }
}

ModalWithTurboStreamTests.registerSuite()
