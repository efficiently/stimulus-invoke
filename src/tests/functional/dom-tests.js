import { FunctionalTestCase } from '../helpers/functional-test-case'

export class DomTests extends FunctionalTestCase {
  async setup() {
    await this.goToLocation('/src/examples/advanced/dom.html')
  }

  async 'test dom open and close'() {
    await this.nextBeat
    const dom = await this.querySelector('#menu')
    this.assert.exists(dom)
    this.assert.isNull(await dom.getAttribute('open'))

    await this.clickSelector('button#btn_open')
    this.assert.isNotNull(await dom.getAttribute('open'))

    await this.clickSelector('button#btn_close')
    this.assert.isNull(await dom.getAttribute('open'))

    await this.clickSelector('button#btn_toggle')
    this.assert.isNotNull(await dom.getAttribute('open'))

    await this.clickSelector('button#btn_toggle')
    this.assert.isNull(await dom.getAttribute('open'))
  }
}

DomTests.registerSuite()
