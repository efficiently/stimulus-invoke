import { FunctionalTestCase } from '../helpers/functional-test-case'

export class SelectboxTests extends FunctionalTestCase {
  async setup() {
    await this.goToLocation('/src/examples/advanced/selectbox.html')
  }

  async 'test selectbox open and close'() {
    await this.nextBeat
    const controller = await this.querySelector('[data-controller="selectbox"]')
    this.assert.exists(controller)

    const selectboxUid = await controller.getAttribute('data-ssid')
    this.assert.isString(selectboxUid)

    const selectbox = await this.querySelector(`.${selectboxUid}`)
    this.assert.exists(selectbox)
    this.assert.notInclude(await selectbox.getVisibleText(), 'One\nTwo\nThree')

    await this.clickSelector('button#btn_open')
    await this.sleep(250)
    this.assert.include(await selectbox.getVisibleText(), 'One\nTwo\nThree')

    await this.clickSelector('button#btn_close')
    await this.sleep(250)
    this.assert.notInclude(await selectbox.getVisibleText(), 'One\nTwo\nThree')

    await this.clickSelector('button#btn_open_search')
    await this.sleep(250)
    this.assert.include(await selectbox.getVisibleText(), 'Two')
    this.assert.notInclude(await selectbox.getVisibleText(), 'One\nTwo\nThree')
  }
}

SelectboxTests.registerSuite()
