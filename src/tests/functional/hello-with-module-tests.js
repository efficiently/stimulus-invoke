import { FunctionalTestCase } from '../helpers/functional-test-case'

export class HelloWithModuleTests extends FunctionalTestCase {
  async setup() {
    await this.goToLocation('/src/examples/basic/hello-with-module.html')
  }

  async 'test hello with module#world'() {
    await this.sleep(500)
    const input = await this.querySelector('input[data-hello-with-module-target~=name]')
    this.assert.exists(input)
    this.assert.equal(await input.getProperty('value'), 'hello-with-module#world method invoked!')
  }
}

HelloWithModuleTests.registerSuite()
