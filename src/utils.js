export function camelCase(str) {
  return str.replace(/([_\s\t-])+([A-z])/g, (match, p1, p2) => p2.toUpperCase())
}

export function addAttributeValueFrom(element, attributeName, attributeValue) {
  if (!element.matches(`[${attributeName}~="${attributeValue}"]`)) {
    element.setAttribute(attributeName, ((element.getAttribute(attributeName) || '') + ' ' + attributeValue).trim())
  }
}

export function removeAttributeValueFrom(element, attributeName, attributeValue) {
  if (element.matches(`[${attributeName}~="${attributeValue}"]`)) {
    element.setAttribute(
      attributeName,
      (element.getAttribute(attributeName) || '')
        .split(' ')
        .filter((value) => value.trim() !== attributeValue)
        .join(' ')
    )
  }
}

export function findElement(selector) {
  if (selector instanceof Element) {
    return selector
  }

  return document.getElementById(selector) || document.querySelector(selector)
}

// Try to get the element by first looking it up by ID.
// If that fails, try using the selector as a CSS Selector.
export function findElements(selector) {
  if (selector instanceof Element) {
    return [selector]
  }

  const idTarget = document.getElementById(selector)
  if (idTarget) {
    return [idTarget]
  }

  const cssTarget = document.querySelectorAll(selector)
  if (cssTarget.length) {
    return [...cssTarget]
  }

  return []
}

// Borrowed from: https://stackoverflow.com/a/63548167
function findPrototypeProperty(object, propertyName) {
  while (object?.constructor?.name !== 'Object') {
    const desc = Object.getOwnPropertyDescriptor(object, propertyName)
    if (desc) {
      return desc
    }
    object = Object.getPrototypeOf(object)
  }

  return null
}

// Test if a DOM element property is read-only or writable
// Usage:
//   const element = document.getElementById('my_element_id')
//   isElementPropertyWritable(element, 'id') // -> true
//   isElementPropertyWritable(element, 'children') // -> false
//   Borrowed from: https://stackoverflow.com/a/63548167
export function isElementPropertyWritable(object, propertyName) {
  const desc = findPrototypeProperty(object, propertyName)
  if (!desc) {
    return false
  }

  if (desc.writable && typeof desc.value !== 'function') {
    return true
  }

  return !!desc.set
}

export function toggleAttributeFrom(element, attributeName, attributeValue) {
  if (element) {
    if (attributeValue) {
      element.setAttribute(attributeName, attributeValue)
    } else {
      element.removeAttribute(attributeName)
    }
  }
}

// Stimulus private functions

// Borrowed from https://github.com/hotwired/stimulus/blob/v3.0.1/src/core/action.ts
const defaultEventNames = {
  'a': (e) => 'click',
  'button': (e) => 'click',
  'details': (e) => 'toggle',
  'form': (e) => 'submit',
  'input': (e) => e.getAttribute('type') == 'submit' ? 'click' : 'input',
  'select': (e) => 'change',
  'textarea': (e) => 'input',
}

export function getDefaultEventNameForElement(element) {
  const tagName = element.tagName.toLowerCase()
  if (tagName in defaultEventNames) {
    return defaultEventNames[tagName](element)
  }
}

// Borrowed from: https://github.com/hotwired/stimulus/blob/v3.0.1/src/core/action_descriptor.ts

const descriptorPattern = /^((.+?)(@(window|document))?->)?(.+?)(#([^:]+?))(:(.+))?$/

export function parseActionDescriptorString(descriptorString) {
  const source = descriptorString.trim()
  const matches = source.match(descriptorPattern) || []

  return {
    eventTarget: parseEventTarget(matches[4]),
    eventName: matches[2],
    eventOptions: matches[9] ? parseEventOptions(matches[9]) : {},
    identifier: matches[5],
    methodName: matches[7],
  }
}

function parseEventTarget(eventTargetName) {
  if (eventTargetName == 'window') {
    return window
  } else if (eventTargetName == 'document') {
    return document
  }
}

function parseEventOptions(eventOptions) {
  return eventOptions.split(':').reduce((options, token) =>
    Object.assign(options, { [token.replace(/^!/, '')]: !/^!/.test(token) })
  , {})
}
